import 'package:flutter/material.dart';

class SizeConverter{
  static const Size imaginedScreenSize = const Size(360.0, 740.0);
  Size size ;
  SizeConverter(BuildContext context){
    this.size = MediaQuery.of(context).size;
  }

  double getHeight(double height){
    return size.height * (height / imaginedScreenSize.height);
  }

  double getWidth(double width){
    return size.width * (width / imaginedScreenSize.width);
  }
}
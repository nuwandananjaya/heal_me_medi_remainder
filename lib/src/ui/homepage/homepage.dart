import 'package:flutter/material.dart';
import 'package:heal_me_medi_remainder/src/global_bloc.dart';
import 'package:heal_me_medi_remainder/src/models/medicine.dart';
import 'package:heal_me_medi_remainder/src/ui/new_entry/new_entry.dart';
import 'package:heal_me_medi_remainder/src/utill/const.dart';
import 'package:heal_me_medi_remainder/src/utill/media_query_size_converter.dart';
import 'package:heal_me_medi_remainder/src/widgets/card_items.dart';
import 'package:heal_me_medi_remainder/src/widgets/card_section.dart';
import 'package:heal_me_medi_remainder/src/widgets/custom_clipper.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  void initState() {
    super.initState();
  }

  /*@override
  Widget build(BuildContext context) {
    final GlobalBloc _globalBloc = Provider.of<GlobalBloc>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF3EB16F),
        elevation: 0.0,
      ),
      body: Container(
        color: Color(0xFFF6F8FC),
        child: Column(
          children: <Widget>[
            Flexible(
              flex: 3,
              child: TopContainer(),
            ),
            SizedBox(
              height: 10,
            ),
            Flexible(
              flex: 7,
              child: Provider<GlobalBloc>.value(
                child: BottomContainer(),
                value: _globalBloc,
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        elevation: 4,
        backgroundColor: Color(0xFF3EB16F),
        child: Icon(
          Icons.add,
        ),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => NewEntry(),
            ),
          );
        },
      ),
    );
  }*/

  @override
  Widget build(BuildContext context) {
    double statusBarHeight = MediaQuery.of(context).padding.top;
    final GlobalBloc _globalBloc = Provider.of<GlobalBloc>(context);

    return Scaffold(
      backgroundColor: Constants.backgroundColor,
      body: Stack(
        children: <Widget>[
          ClipPath(
            clipper: MyCustomClipper(clipType: ClipType.bottom),
            child: Container(
              color: Theme.of(context).accentColor,
              height: Constants.headerHeight + statusBarHeight,
            ),
          ),
          Positioned(
            right: -45,
            top: -30,
            child: ClipOval(
              child: Container(
                color: Colors.black.withOpacity(0.05),
                height: 220,
                width: 220,
              ),
            ),
          ),

          // BODY
          Padding(
            padding: EdgeInsets.all(Constants.paddingSide),
            child: ListView(
              children: <Widget>[
                // Header - Greetings and Avatar
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        "Be Safe,\nMy Friend",
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.w900,
                            color: Colors.white),
                      ),
                    ),

                    Image(
                        height: 50,
                        width: 50,
                        image: AssetImage('assets/icons/profile_picture.png'))
                    //CircleAvatar(
                    //   radius: 26.0,
                    //   backgroundImage:
                    //     AssetImage('assets/icons/profile_picture.png'))
                  ],
                ),

                // SizedBox(height: 50),

                // Main Cards - Heartbeat and Blood Pressure
                /* Container(
                  height: 140,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      CardMain(
                        image: AssetImage('assets/icons/heartbeat.png'),
                        title: "Hearbeat",
                        value: "66",
                        unit: "bpm",
                        color: Constants.lightGreen,
                      ),
                      CardMain(
                          image: AssetImage('assets/icons/blooddrop.png'),
                          title: "Blood Pressure",
                          value: "66/123",
                          unit: "mmHg",
                          color: Constants.lightYellow)
                    ],
                  ),
                ),*/

                // Section Cards - Daily Medication
                SizedBox(height: 50),

                Text(
                  "SCHEDULED ACTIVITIES",
                  style: TextStyle(
                    color: Constants.textPrimary,
                    fontSize: 13,
                    fontWeight: FontWeight.bold,
                  ),
                ),

                SizedBox(height: 20),

                Container(
                    height: 125,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        CardSection(
                          image: AssetImage('assets/icons/capsule.png'),
                          title: "Metforminv",
                          value: "2",
                          unit: "pills",
                          time: "6-7AM",
                          isDone: false,
                        ),
                        CardSection(
                          image: AssetImage('assets/icons/syringe.png'),
                          title: "Trulicity",
                          value: "1",
                          unit: "shot",
                          time: "8-9AM",
                          isDone: true,
                        )
                      ],
                    )),

                SizedBox(height: 50),

                // Scheduled Activities
                Text(
                  "YOUR DAILY MEDICATIONS",
                  style: TextStyle(
                      color: Constants.textPrimary,
                      fontSize: 13,
                      fontWeight: FontWeight.bold),
                ),

                SizedBox(height: 20),

                Container(
                    child: Provider<GlobalBloc>.value(
                  child: MediContainer(),
                  value: _globalBloc,
                ))

                /* Container(
                  child: ListView(
                    scrollDirection: Axis.vertical,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    children: <Widget>[
                      CardItems(
                        image: Image.asset(
                          'assets/icons/Walking.png',
                        ),
                        title: "Walking",
                        value: "750",
                        unit: "steps",
                        color: Constants.lightYellow,
                        progress: 30,
                      ),
                      CardItems(
                        image: Image.asset(
                          'assets/icons/Swimming.png',
                        ),
                        title: "Swimming",
                        value: "30",
                        unit: "mins",
                        color: Constants.lightBlue,
                        progress: 0,
                      ),
                    ],
                  ),
                ),*/
              ],
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        elevation: 4,
        backgroundColor: Color(0xFF3EB16F),
        child: Icon(
          Icons.add,
        ),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => NewEntry(),
            ),
          );
        },
      ),
    );
  }
}

class MediContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final GlobalBloc _globalBloc = Provider.of<GlobalBloc>(context);
    return StreamBuilder<List<Medicine>>(
      stream: _globalBloc.medicineList$,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Container();
        } else if (snapshot.data.length == 0) {
          return Container(
            color: Color(0xFFF6F8FC),
            child: Center(
              child: Text(
                "Press + to add a Medi Reminder",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: SizeConverter(context).getWidth(25),
                    color: Constants.textPrimary,
                    fontWeight: FontWeight.bold),
              ),
            ),
          );
        } else {
          return ListView.builder(
            itemCount: snapshot.data.length,
            scrollDirection: Axis.vertical,
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (context, index) {
              //return MedicineCard(snapshot.data[index]);
              var medicine = snapshot.data[index];
              Color color = Color(0xFF3EB16F);
              IconData iconData;
              if (medicine.medicineType == "Bottle") {
                iconData = IconData(0xe900, fontFamily: "Ic");
              } else if (medicine.medicineType == "Pill") {
                iconData = IconData(0xe901, fontFamily: "Ic");
              } else if (medicine.medicineType == "Syringe") {
                iconData = IconData(0xe902, fontFamily: "Ic");
              } else if (medicine.medicineType == "Tablet") {
                iconData = IconData(0xe903, fontFamily: "Ic");
              }
              return CardItems(
                image: iconData,
                title: "Swimming",
                value: "30",
                unit: "mins",
                color: color,
                progress: 0,
              );
            },
          );
        }
      },
    );
  }
}

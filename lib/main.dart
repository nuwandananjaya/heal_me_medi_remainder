import 'package:device_preview/device_preview.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:heal_me_medi_remainder/src/global_bloc.dart';
import 'package:heal_me_medi_remainder/src/ui/homepage/homepage.dart';
import 'package:provider/provider.dart';

// void main() {
//   runApp(MedicineReminder());
// }
//
// class MedicineReminder extends StatefulWidget {
//   @override
//   _MedicineReminderState createState() => _MedicineReminderState();
// }
//
// class _MedicineReminderState extends State<MedicineReminder> {
//   GlobalBloc globalBloc;
//
//   void initState() {
//     globalBloc = GlobalBloc();
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Provider<GlobalBloc>.value(
//       value: globalBloc,
//       child: MaterialApp(
//         theme: ThemeData(
//           primarySwatch: Colors.green,
//           brightness: Brightness.light,
//         ),
//         home: HomePage(),
//         debugShowCheckedModeBanner: false,
//       ),
//     );
//   }
// }

/**
 * with device preview
 */
void main() {
  //
  runApp(
    DevicePreview(
      enabled: !kReleaseMode,
      builder: (context) => MedicineReminder(),
    ),
  );
  //runApp(MedicineReminder());
}

class MedicineReminder extends StatefulWidget {
  @override
  _MedicineReminderState createState() => _MedicineReminderState();
}

class _MedicineReminderState extends State<MedicineReminder> {
  GlobalBloc globalBloc;

  void initState() {
    globalBloc = GlobalBloc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Provider<GlobalBloc>.value(
      value: globalBloc,
      child: MaterialApp(
        locale: DevicePreview.locale(context),
        builder: DevicePreview.appBuilder,
        theme: ThemeData(
          primarySwatch: Colors.green,
          brightness: Brightness.light,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: HomePage(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
